from urllib import response
from flask import Flask, render_template, request, redirect, url_for
import requests
import os


app = Flask(__name__)

@app.route('/')
def index():
    # return render_template('index.html', landing_image=requests.get('https://api.nasa.gov/planetary/apod?api_key=Tqjn85RglAK2x2gZAjniHRLhsZdFfluOO5XkiJX7'))
    # image = requests.get('https://api.github.com/events')
    # print(image.json())
    # return render_template('index.html', landing_image=image.json())

    # api_key = os.getenv('API_KEY')
    # response = requests.get('https://api.nasa.gov/planetary/apod?api_key={}'.format(api_key))
    # print(response.json())
    # return render_template('index.html', landing_image=response.json())


    trello_key = os.getenv('TRELLO_KEY')
    trello_token = os.getenv('TRELLO_TOKEN')
    trello_response = requests.get(f"https://api.trello.com/1/boards/h3kwdx7f?key={trello_key}&token={trello_token}&cards=all")
    print(trello_response.status_code)
    print(trello_response.json())


    api_key1 = os.getenv('API_KEY1')
    # response = requests.get('https://api.nasa.gov/planetary/apod?api_key={}'.format(api_key1))
    response = requests.get(f"https://api.nasa.gov/planetary/apod?api_key={api_key1}")
    return render_template('index.html', landing_image=response.json())




@app.route('/mars')
def mars():
    return render_template('mars.html')